import 'package:flutter/material.dart';
import 'dart:async';

class Stopwatch extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return StopwatchState();
  }
}

class StopwatchState extends State<StatefulWidget> {
  int milliseconds = 0;
  int seconds = 0;
  late Timer timer;
  final laps = <int>[];
  bool isTicking = false;
  final itemHeight = 60.0;
  // ScrollController not attached to any scroll views.
  final scrollController = ScrollController();

  @override
  Widget build(BuildContext context) {
    return Container(
        color: Theme.of(context).primaryColor,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              'Lap: ${laps.length + 1}',
              style: Theme.of(context)
                  .textTheme
                  .subtitle1!
                  .copyWith(color: Colors.white),
            ),
            Text(
              _secondsText(milliseconds),
              style: Theme.of(context)
                  .textTheme
                  .headline5!
                  .copyWith(color: Colors.white),
            ),
            const SizedBox(height: 22),
            _buildControls()
          ],
        ));
  }

  Row _buildControls() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        ElevatedButton(
            style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all<Color>(Colors.green),
                foregroundColor:
                    MaterialStateProperty.all<Color>(Colors.white)),
            onPressed: isTicking ? null : _startTimer,
            child: const Text('Start')),
        const SizedBox(width: 20),
        ElevatedButton(
            style: ButtonStyle(
                backgroundColor:
                    MaterialStateProperty.all<Color>(Colors.yellow),
                foregroundColor:
                    MaterialStateProperty.all<Color>(Colors.black54)),
            onPressed: isTicking ? _lap : null,
            child: const Text('Lap')),
        const SizedBox(width: 20),
        Builder(
            builder: (context) => TextButton(
                  style: ButtonStyle(
                    backgroundColor:
                        MaterialStateProperty.all<Color>(Colors.red),
                    foregroundColor:
                        MaterialStateProperty.all<Color>(Colors.white),
                  ),
                  child: const Text('Stop'),
                  onPressed: isTicking ? () => _stopTimer(context) : null,
                ))
      ],
    );
  }

  String _secondsText(int milliseconds) {
    final seconds = milliseconds / 1000;
    return '$seconds seconds';
  }

  void _startTimer() {
    timer = Timer.periodic(const Duration(milliseconds: 100), _onTick);
    setState(() {
      laps.clear();
      isTicking = true;
    });
  }

  void _stopTimer(BuildContext context) {
    timer.cancel();
    setState(() {
      isTicking = false;
    });
  }

  void _onTick(Timer time) {
    setState(() {
      milliseconds += 100;
    });
  }

  void _lap() {
    scrollController.animateTo(
      itemHeight * laps.length,
      duration: const Duration(milliseconds: 1000),
      curve: Curves.easeIn,
    );
    setState(() {
      laps.add(milliseconds);
      milliseconds = 0;
    });
  }
}
